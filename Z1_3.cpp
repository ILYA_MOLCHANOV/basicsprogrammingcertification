// Z1_3.cpp: ���������� ����� ����� ��� ����������� ����������.


#include "stdafx.h"

#include <iostream>
#include <cmath>


int main() {
    const double h = 0.2; // ���
    double x = -4.0;      // ��������� �������� x

    std::cout << "x\t\tf(x)" << std::endl;
    while (x <= 4.0) { // �������� �� -4 �� 4 � ����� h
        // ��������� �������, ������� ������� �� ����
        if (x == -2.0) {
            std::cout << x << "\t\t" << "undefined" << std::endl;
        } else {
            std::cout << x << "\t\t" << sin(x) / (2 + x) << std::endl;
        }
        x += h; // ����������� x �� ��� h
    }

    return 0;
}

