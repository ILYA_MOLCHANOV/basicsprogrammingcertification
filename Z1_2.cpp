// Z1_2.cpp: ���������� ����� ����� ��� ����������� ����������.


#include "stdafx.h"

#include <iostream>
#include <cmath>

// ������� ��� �������� ����� ����
double sumOfSeries(int n) {
    double sum = 0.0; // ���������� ��� double ��� ������� ����������

    for (int k = 1; k <= n; ++k) {
        // ���������� ����� ���� � ���������� ��������������� �������������
        sum += pow(-1, k + 1) / (k * (k + 1)); 
    }

    return sum;
}

int main() {
	setlocale(LC_ALL, "Russian");
    int n;
    std::cout << "������� n: ";
    std::cin >> n; // ���� �������� n ��������������

    double result = sumOfSeries(n);
    std::cout << "����� ���� �����: " << result << std::endl;

    return 0;
}
