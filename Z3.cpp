// Z3.cpp: ���������� ����� ����� ��� ����������� ����������.

#include "stdafx.h"
#include <iostream>
#include <string>

// ������� ����� Person
class Person {
protected:
    std::string name;
    int age;

public:
    Person(std::string name, int age) : name(name), age(age) {}

    void setName(std::string newName) { name = newName; }
    void setAge(int newAge) { age = newAge; }
    std::string getName() const { return name; }
    int getAge() const { return age; }

    virtual void display() {
        std::cout << "Name: " << name << ", Age: " << age << std::endl;
    }
};

// ����� Employee, ��������� Person
class Employee : public Person {
protected:
    std::string position;

public:
    Employee(std::string name, int age, std::string position)
        : Person(name, age), position(position) {}

    void setPosition(std::string newPosition) { position = newPosition; }
    std::string getPosition() const { return position; }

    void display() override {
        Person::display();
        std::cout << "Position: " << position << std::endl;
    }
};

// ����� Worker, ��������� Employee
class Worker : public Employee {
public:
    Worker(std::string name, int age, std::string position)
        : Employee(name, age, position) {}

    void display() override {
        Employee::display();
        std::cout << "I am a worker." << std::endl;
    }
};

// ����� Student, ��������� Person
class Student : public Person {
private:
    std::string university;

public:
    Student(std::string name, int age, std::string university)
        : Person(name, age), university(university) {}

    void setUniversity(std::string newUniversity) { university = newUniversity; }
    std::string getUniversity() const { return university; }

    void display() override {
        Person::display();
        std::cout << "University: " << university << std::endl;
    }
};

// ����� Librarian, ��������� Employee
class Librarian : public Employee {
public:
    Librarian(std::string name, int age, std::string position)
        : Employee(name, age, position) {}

    void display() override {
        Employee::display();
        std::cout << "I am a librarian." << std::endl;
    }
};

// ����� Director, ��������� Employee
class Director : public Employee {
public:
    Director(std::string name, int age, std::string position)
        : Employee(name, age, position) {}

    void display() override {
        Employee::display();
        std::cout << "I am the director." << std::endl;
    }
};

// ����� Staff, ��������� Person
class Staff : public Person {
public:
    Staff(std::string name, int age) : Person(name, age) {}

    void display() override {
        Person::display();
        std::cout << "I am staff." << std::endl;
    }
};

// ���������������� ������� main
int main() {
    Student student("Ilai Hon", 20, "Harvard");
    Worker worker("Jan Dilon", 30, "Engineer");
    Librarian librarian("Emma Brown", 35, "Librarian");
    Director director("Richard Miles", 50, "Director");
    Staff staff("Alice Smith", 28);

    student.display();
    worker.display();
    librarian.display();
    director.display();
    staff.display();

    return 0;
}
