// Z1_1.cpp: ���������� ����� ����� ��� ����������� ����������.
#include "stdafx.h"
#include <iostream>

// ������� ��� �������� ����� �����, ������� ����, � �������� ���������
int sumOf(int start, int end) {
    int sum = 0;
    for (int i = start; i <= end; i++) {
        if (i % 3 == 0) {
            sum += i;
        }
    }
    return sum;
}

// �������� �������
int main() {

	setlocale(LC_ALL, "Russian");
    int start = 1;
    int end = 50;

    int sum = sumOf(start, end);
    std::cout << "����� ���� �����, ������� ���� � ��������� [" << start << ", " << end << "] ����� " << sum << std::endl;

    return 0;
}


