// Z2.cpp: ���������� ����� ����� ��� ����������� ����������.


#include "stdafx.h"

#include <iostream>

// ������� ��� �������� � ������������� ����������� �������
double** createMatrix(int n) {
    double** matrix = new double*[n];
    for (int i = 0; i < n; ++i) {
        matrix[i] = new double[n];
        for (int j = 0; j < n; ++j) {
            std::cout << "������� ������� A[" << i << "][" << j << "]: ";
            std::cin >> matrix[i][j];
        }
    }
    return matrix;
}

// ������� ��� ��������� ��������� �������
void processMatrix(double** matrix, int n) {
    for (int i = 0; i < n; ++i) {
        double diagonalElement = matrix[i][n - 1 - i]; // ������� �������� ���������
        for (int j = 0; j < n; ++j) {
            matrix[i][j] += diagonalElement;
        }
    }
}

// ������� ��� ������ ����������� �������
void printMatrix(double** matrix, int n) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            std::cout << matrix[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

// ������� ��� ������������ ������ ����������� �������
void deleteMatrix(double** matrix, int n) {
    for (int i = 0; i < n; ++i) {
        delete[] matrix[i];
    }
    delete[] matrix;
}

// ������� ��� ������ ���������� ��������� ����������� �������
void printInitialMatrix(double** matrix, int n) {
    std::cout << "��������� ������:" << std::endl;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            std::cout << matrix[i][j] << " ";
        }
        std::cout << std::endl;
    }
}


// �������� �������
int main() {
	setlocale(LC_ALL, "Russian");
    int n;
    std::cout << "������� ������ ������� N: ";
    std::cin >> n;

    double** matrix = createMatrix(n); // ������� �������
	printInitialMatrix(matrix, n);     // ������� ��������� ������
    processMatrix(matrix, n);          // ������������ �������
    std::cout << "��������������� �������:" << std::endl;
    printMatrix(matrix, n);            // ������� �������
    deleteMatrix(matrix, n);           // ����������� ������

    return 0;
}



